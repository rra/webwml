#use wml::debian::template title="Errata corrige dell'Installatore Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="7c890b56dabd473e1766da76b12df99162d5afcb" maintainer="Luca Monducci" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"


<h1>Errata per <humanversion /></h1>


<p>
Questo è l'elenco dei problemi conosciuti presenti nella versione <humanversion />
dell'Installatore Debian. Chi riscontra un problema non ancora presente
in questa pagina è invitato a inviare un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">resoconto
dell'installazione</a> in cui viene descritto il problema.
</p>


<dl class="gloss">
	<dt>Tema grafico usato nell'installatore</dt>
	<dd>Ancora non c'è un tema grafico per Bookworm, l'installatore
	continua a utilizzare il tema di Bullseye.</dd>

	<dt>Alcune schede sonore richiedono un firmware</dt>
	<dd>A quanto pare c'è un certo numero di schede audio
	che richiedono il caricamento del proprio firmware per emettere
	dei suoni. Al momento l'installatore non è in grado di caricarli in
	tempo e questo comporta che non è possibile usare la sintesi vocale
	durante l'installazione. Per aggirare questo problema è possibile
	aggiungere un'altra scheda sonora che non ha bisogno di firmware.
	Consultare la <a href="https://bugs.debian.org/992699">segnalazione
	di bug ombrello</a> per tener traccia delle attività.</dd>

	<dt>L'installazione desktop potrebbe non funzionare con il solo CD#1</dt>
	<dd>Poiché lo spazio disponibile sul primo CD è limitato, non tutti i
	pacchetti necessari per l'ambiente desktop GNOME sono presenti nel CD.
	Per completare l'installazione è necessario usare anche altri
	supporti (per esempio un secondo CD oppure un mirror in rete).<br />
	<b>Stato:</b> È improbabile che il lavoro per far entrare altri
	pacchetti nel CD#1 continui.</dd>

	<dt>LUKS2 è incompatibile con il supporto cryptodisk di GRUB</dt>
	<dd>È da poco stato scoperto che GRUB non supporta LUKS2. Ciò comporta
	che gli utenti che vogliono usare <tt>GRUB_ENABLE_CRYPTODISK</tt>
	devono inevitabilmente avere una direcotry <tt>/boot</tt> separata e
	non criptata (<a href="https://bugs.debian.org/927165">#927165</a>).
	Questa configurazione non è supportata dall'installatore ma merita
	comunque di esssere documentata in modo più prominente ed è opportuno
	permettere almeno la possibilità di scegliere LUKS1 durante il processo
	d'installazione.
	<br />
	<b>Stato:</b> Sono state raccolte alcune idee riguardo a questo bug;
	i manutentori di cryptsetup hanno praparato della <a
	href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">documentazione
	specifica</a>.</dd>

</dl>
