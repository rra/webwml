#use wml::debian::cdimage title="Obrazy instalacyjne live"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7"

<p>Obraz instalacyjny <q>live</q> zawiera system Debian, który można uruchomić bez
zmieniania czegokolwiek na twardym dysku, i który może także posłużyć do instalacji Debiana.
</p>

<p><a name="choose_live"><strong>Czy obraz <q>live</q> jest odpowiedni dla mnie?</strong></a> Oto kilka rzeczy które pomogą Ci zdecydować.
<ul>
<li><b>Warianty:</b> Istnieje kilka wariantów obrazów <q>live</q> 
umożliwiających wybór środowiska graficznego (GNOME, KDE, LXDE, Xfce, Cinnamon
oraz MATE). Dla wielu użytkowników odpowiedni będzie początkowy wybór pakietów, 
jakkolwiek mogą oni potem doinstalować z sieci pakiety, których potrzebują. 
<li><b>Architektura:</b> Na chwilę obecną dostępne są tylko obrazy dla dwóch
najpopularniejszych architektur: 32-bitowych PC (i386) oraz 64-bitowych PC
(amd64).
<li><b>Instalator:</b>Począwszy od Debiana 10 Buster obrazy live zawierają 
przyjazny dla użytkownika końcowego <a href="https://calamares.io">Instalator Calamares</a>, 
niezależny od dystrybucji instalator, jako alternatywę do naszego, dobrze znanego 
<a href="$(HOME)/devel/debian-installer">Instalatora Debiana</a>.
<li><b>Rozmiar:</b> Każdy z obrazów jest o wiele mniejszy niż pełny zestaw
obrazów DVD, ale większy niż nośniki do instalacji z sieci.
<li><b>Języki:</b> Te obrazy nie zawierają pełnego zestawu pakietów obsługi rożnych języków.
Jeśli potrzebujesz zaawansowanych systemów wprowadzania znaków, czcionek i
dodatkowych pakietów obsługi języków, będziesz musiał(a) zainstalować je
później.
</ul>

<p>Możesz pobrać następujące obrazy instalacyjne <q>live</q>:</p>
 
<ul>

  <li>Oficjalne obrazy instalacyjne <q>live</q> wydania <q>stabilnego</q> &mdash; <a
  href="#live-install-stable">patrz niżej</a></li>

</ul>


<h2 id="live-install-stable">Oficjalne obrazy instalacyjne <q>live</q> wydania <q>stabilnego</q></h2>

<p>Dostępne w kilku wariantach o rożnych rozmiarach wspomnianych powyżej,
obrazy te pozwalają wypróbować system Debian złożony z wybranego zestawu
pakietów, a następnie zainstalować go z tego samego nośnika.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (przez <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>Hybrydowe</q> obrazy ISO nadające się do zapisu na nośnikach
DVD-R(W) a także na napędach USB odpowiedniej wielkości.
Prosimy, o ile to możliwe, korzystać z BitTorrenta &mdash; pozwala to
zmniejszyć obciążenie naszych serwerów.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"><p><strong>DVD/USB</strong></p>
<p><q>Hybrydowe</q> obrazy ISO nadające się do zapisu na nośnikach DVD-R(W) 
a także na napędach USB odpowiedniej wielkości.</p>
	  <stable-live-install-iso-cd-images />
</div>
</div>

<p>Informacje na temat tych plików i ich użycia dostępne są w <a
href="../faq/">FAQ</a>.</p>

<p>Jeżeli Debian ma być zainstalowany z pobranego obrazu <q>live</q>,
zalecamy przeczytanie <a
href="$(HOME)/releases/stable/installmanual">szczegółowych informacji na temat
procesu instalacji</a>.</p>

<p>Więcej informacji na temat systemów Debian Live udostępnianych na tych
obrazach można znaleźć na <a href="$(HOME)/devel/debian-live">stronie projektu
Debian Live</a>.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Nieoficjalne obrazy live CD/DVD z dołączonym niewolnym oprogramowaniem firmware</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego 
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w
<a href="../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy live dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>