msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-04-17 09:11+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistikk for oversettelsen av Debian sitt nettsted"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Det er %d sider å oversette."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Det er %d byte å oversette."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Det er %d strenger å oversette."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Feil oversettelsesversjon"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Denne oversettelsen for utdatert"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Originalen er nyere enn denne oversettelsen"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Originalen fins ikke lenger"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "antall treff fins ikke"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "treff"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr ""

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Oversettelsessammendrag for"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Ikke oversatt"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Utdatert"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Oversatt"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Oppdatert"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "byte"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Merk: Lista med sider er sortert etter popularitet. Hold musepekeren over "
"sidenavnet for å se antall treff."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Utdaterte oversettelser"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr ""

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr ""

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Logg"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Oversettelse"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Vedlikeholder"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Oversetter"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Dato"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Generelle sider som ikke er oversatt"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Ikke-oversatte generelle sider"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nyhetsoppføringer som ikke er oversatt"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Ikke-oversatte nyhetsoppføringer"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Konsulent/brukersider som ikke er oversatt"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Ikke-oversatte konsulent/brukersider"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Internasjonale sider som ikke er oversatt"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Ikke-oversatte internasjonale sider"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Oversatte sider (oppdatert)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Oversatte maler (PO-filer)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO-oversettelsesstatistikk"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Ikke oversatt"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Totalt"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Totalt:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Oversatte nettsider"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Oversettelsesstatistikk etter sideantall"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Språk"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Oversettelser"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Oversatte nettsider (etter størrelse)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Oversettelsesstatistikk etter sidestørrelse"

#~ msgid "Created with"
#~ msgstr "Opprettet med"

#~ msgid "Origin"
#~ msgstr "Kilde"
