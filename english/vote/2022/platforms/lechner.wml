#use wml::debian::template title="Platform for Felix Lechner" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<h1>Better Happy!</h1>

<p>
Hi, welcome to my platform to become Debian Project Leader in 2022.
I would like to offer you my proven leadership skills to elevate the happiness
and well-being of all Debianites for the coming year.
</p>

<h2>
Let's meet
</h2>

<p>
Thanks to the help of a friend, you can now find me on OFTC in <code>#meetfelix</code>.
I'm also on <a href="https://jitsi.debian.social/MeetFelix">Jitsi</a> often.
</p>

<h2>
Compassion for one another
</h2>

<p>
Our work is often strenuous and lonely, but together we can make a difference.
On a technical level, Debian already does so for millions of users every day.
On a human level, however, we often fall short.
Maybe someone is bitter because they did not buy bitcoin, or
maybe they did and their significant other left anyway.
(I own no crypto.)
Either way,
<a href="https://www.youtube.com/watch?v=36m1o-tM05g">happiness</a>
is the most precious gift.
Let's make Debian a happy place together.
</p>

<h2>
A solid foundation
</h2>

<p>
My approach is informed by a bunch of new-agey ideas like those of
Eckhart Tolle
(<a href="https://en.wikipedia.org/wiki/The_Power_of_Now">here</a>
and
<a href="https://en.wikipedia.org/wiki/A_New_Earth">here</a>)
coupled with some good old religion.
In some sense I seek to rejuvenate Debian, which is an odd thing to write
since I'll probably be the oldest project leader ever.
What I mean, though, is to leave aside the cynicism and the anger that take
center stage in Debian sometimes.
Let me be your teacher!
</p>

<p>
Why believe me? It's easy.
</p>

<h2>
A trusted leader
</h2>

<p>
For nearly nine years, I have been a minor city official&mdash;a
<a href="https://www.fremont.gov/1480/Library-Advisory-Commission">library commissioner</a>
to be exact&mdash;in a special place.
My chosen home of Fremont, Calif., is the
<a href="https://pluralism.org/fremont-usa">most diverse town</a>
in America.
My wife is from China. My best friend is from India.
In some ways, Fremont is a model of what Debian might be.
</p>

<p>
To top it off, Fremont has been the
<a href="https://wallethub.com/edu/happiest-places-to-live/32619">happiest community</a>
in North America for many years (although I have yet to see a comparison to
Denmark).
One likely cause? Fremont has a civic system of boards and commissions that means
everyone gets a voice.
</p>

<p>
If you elect me, that's what we will do for Debian together. We will
give Debian a republic!
</p>

<h2>
Please call
</h2>

<p>
As your leader, I will strive to be the most available official ever.
While I enjoy writing, I prefer phone calls and face-to-face meetings.
I hope to be available nearly daily on <a href="https://jitsi.debian.social/MeetFelix">Jitsi</a>
(although never on Saturdays).
Otherwise, just call or text.
My number is <a href="tel:+16172295005">+1-617-229-5005</a>.
Everyone is welcome!
</p>

<h2>
An underdog for life
</h2>

<p>
I have been a foreigner nearly all my life. After high school, I left
my native Berlin hoping to study theoretical physics. Instead, I ended
up with a degree in Electrical Engineering and Computer Systems from
Harvard University. My specialty was chip design. I have lived in
Switzerland, China, and Britain. I always needed to fit in, and in some
ways never have.
</p>

<h2>
Your key is expiring
</h2>

<p>
In Debian, my most popular service is without a doubt my key
<a href="https://bugs.debian.org/892058">expiration reminders</a>.
Together with a large number of
<a href="https://salsa.debian.org/groups/lintian/-/group_members">committers</a>,
I also co-maintain
<a href="https://salsa.debian.org/lintian/lintian">Lintian</a> and
<a href="https://lintian.debian.org/">its website</a>, but people appreciate that work
much less.
(David Bremner made me feel better when he gently reminded someone that Lintian
was not their boss.)
I also maintain <code>mdadm</code>, <code>gocryptfs</code>, <code>wolfssl</code>,
and some other smaller packages.
</p>

<h2>
Fully functional
</h2>

<p>
More recently, I gave Haskell a Debhelper
<a href="https://bugs.debian.org/1002296">build system</a>
and took over the Hackage
<a href="https://hackage.debian.net/">version tracker</a> from Joachim Breitner.
As your leader, that would be my greatest distraction.
I am obsessed with Haskell, which is kind of sad because I am not very good at
it&mdash;but as Clint Adams may tell you, I am learning.
</p>

<h2>
Fighting for Debian
</h2>

<p>
Thanks to Sam Hartman's and Brian Gupta's trust, I have been part of
the <a href="https://www.debian.org/trademark">trademark team</a>, a delegated position.
In that capacity, I have worked hard to protect Debian's
<a href="https://tsdr.uspto.gov/#caseNumber=86037470&amp;caseType=SERIAL_NO&amp;searchType=statusSearch">brand</a>
from being co-opted to market proprietary software along with Debian.
</p>

<h2>
My agenda
</h2>

<p>
As your leader, I will work tirelessly to reduce conflict within the project.
Toward the outside world, I will make every effort to help users and specialty
communities around the world fall in love with Debian again. We should
be the premier development platform for all programming ecosystems.
</p>

<p>
I furthermore hope to advance on a variety of long-term challenges for
the project, such as replenishing an aging membership and dealing with
the proliferation of language-specific package managers (aka the
"vendoring" problem). With your help, I hope to put Debian on a good
course for the next ten or so years. Let's work together!
</p>

<h2>
About rebuttals
</h2>

<p>
I am not sure yet whether I'll engage with the
<a href="https://www.debian.org/vote/2022/vote_002">other candidates</a>
on the rebuttal
level. It's probably too <a href="https://www.youtube.com/watch?v=6TmA2XSlQfk">negative</a>.
All of them will do a great job.
Debian is strong.
If you want things to stay the same, just vote for them.
</p>

<h2>
A song for you
</h2>

<p>
If you find this letter boring, please watch my
<a href="https://www.youtube.com/watch?v=RsrxxURtde4">campaign video</a>.
That message is all I should have written here, anyway. Enjoy!
</p>

<p>
Thanks for reading!
</p>

<img alt="Felix Lechner" src="felix.jpg">

