<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Waitress is a Python WSGI server, an application server for Python web apps.</p>

<p>Security updates to fix request smuggling bugs, when combined with another http
proxy that interprets requests differently. This can lead to a potential for
HTTP request smuggling/splitting whereby Waitress may see two requests while
the front-end server only sees a single HTTP message.  This can result in cache
poisoning or unexpected information disclosure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16785">CVE-2019-16785</a>

    <p>Only recognise CRLF as a line-terminator, not a plain LF. Before this
    change waitress could see two requests where the front-end proxy only saw
    one.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16786">CVE-2019-16786</a>

    <p>Waitress would parse the Transfer-Encoding header and only look for a
    single string value, if that value was not <q>chunked</q> it would fall through
    and use the Content-Length header instead.  This could allow for Waitress
    to treat a single request as multiple requests in the case of HTTP
    pipelining.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16789">CVE-2019-16789</a>

    <p>Specially crafted requests containing special whitespace characters in the
    Transfer-Encoding header would get parsed by Waitress as being a chunked
    request, but a front-end server would use the Content-Length instead as the
    Transfer-Encoding header is considered invalid due to containing invalid
    characters.  If a front-end server does HTTP pipelining to a backend
    Waitress server this could lead to HTTP request splitting which may lead to
    potential cache poisoning or unexpected information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16792">CVE-2019-16792</a>

    <p>If two Content-Length headers are sent in a single request, Waitress would
    treat the request as having no body, thereby treating the body of the
    request as a new request in HTTP pipelining.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24761">CVE-2022-24761</a>

    <p>There are two classes of vulnerability that may lead to request smuggling
    that are addressed by this advisory:</p>

    <ol>
        <li>The use of Python's int() to parse strings into integers, leading
        to +10 to be parsed as 10, or 0x01 to be parsed as 1, where as the
        standard specifies that the string should contain only digits or hex
        digits.</li>
        <li>Waitress does not support chunk extensions, however it was
        discarding them without validating that they did not contain illegal
        characters.</li>
    </ol>
</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.1-1+deb9u1.</p>

<p>We recommend that you upgrade your waitress packages.</p>

<p>For the detailed security status of waitress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/waitress">https://security-tracker.debian.org/tracker/waitress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3000.data"
# $Id: $
