<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An out-of-bounds write was discovered in Thunderbird, which could
be triggered via a malformed email message.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:91.6.1-1~deb9u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">https://security-tracker.debian.org/tracker/thunderbird</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2930.data"
# $Id: $
