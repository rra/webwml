<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A set of vulnerabilities was discovered in GnuTLS which allowed
attackers to do plain text recovery on TLS connections with certain
cipher types.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10844">CVE-2018-10844</a>

    <p>It was found that the GnuTLS implementation of HMAC-SHA-256 was
    vulnerable to a Lucky thirteen style attack. Remote attackers
    could use this flaw to conduct distinguishing attacks and
    plaintext-recovery attacks via statistical analysis of timing data
    using crafted packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10845">CVE-2018-10845</a>

    <p>It was found that the GnuTLS implementation of HMAC-SHA-384 was
    vulnerable to a Lucky thirteen style attack. Remote attackers
    could use this flaw to conduct distinguishing attacks and plain
    text recovery attacks via statistical analysis of timing data
    using crafted packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10846">CVE-2018-10846</a>

    <p>A cache-based side channel in GnuTLS implementation that leads to
    plain text recovery in cross-VM attack setting was found. An
    attacker could use a combination of <q>Just in Time</q> Prime+probe
    attack in combination with Lucky-13 attack to recover plain text
    using crafted packets.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.30-0+deb8u1. It was found to be more practical to update to the
latest upstream version of the 3.3.x branch since upstream's fixes
were rather invasive and required cipher list changes anyways. This
will facilitate future LTS updates as well.</p>

<p>This change therefore also includes the following major policy
changes, as documented in the NEWS file:</p>

<ul>

   <li>ARCFOUR (RC4) and SSL 3.0 are no longer included in the default
     priorities list. Those have to be explicitly enabled, e.g., with
     a string like "NORMAL:+ARCFOUR-128" or "NORMAL:+VERS-SSL3.0",
     respectively.</li>

   <li>The ciphers utilizing HMAC-SHA384 and SHA256 have been removed
     from the default priority strings. They are not necessary for
     compatibility or other purpose and provide no advantage over
     their SHA1 counter-parts, as they all depend on the legacy TLS
     CBC block mode.</li>

   <li>Follow closely RFC5280 recommendations and use UTCTime for dates
     prior to 2050.</li>

   <li>Require strict DER encoding for certificates, OCSP requests,
     private keys, CRLs and certificate requests, in order to reduce
     issues due to the complexity of BER rules.</li>

   <li>Refuse to import v1 or v2 certificates that contain extensions.</li>

</ul>

<p>API and ABI compatibility is retained, however, although new symbols
have been added. Many bugfixes are also included in the upload. See
the provided upstream changelog for more details.</p>

<p>We recommend that you upgrade your gnutls28 packages and do not expect
significant breakage.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1560.data"
# $Id: $
