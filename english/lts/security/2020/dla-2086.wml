<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in wget, a tool to retrieve files from the web.
A race condition might occur as files rejected by an access list are kept
on the disk for the duration of a HTTP connection.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.16-1+deb8u7.</p>

<p>We recommend that you upgrade your wget packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2086.data"
# $Id: $
