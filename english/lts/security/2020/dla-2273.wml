<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was two issues in shiro, a security framework
for Java application:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1957">CVE-2020-1957</a>

    <p>Apache Shiro before 1.5.2, when using Apache Shiro with Spring dynamic
    controllers, a specially crafted request may cause an authentication
    bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11989">CVE-2020-11989</a>

    <p>Apache Shiro before 1.5.3, when using Apache Shiro with Spring dynamic
    controllers, a specially crafted request may cause an authentication
    bypass.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.3.2-1+deb9u1.</p>

<p>We recommend that you upgrade your shiro packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2273.data"
# $Id: $
