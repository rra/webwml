<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in xorg-server, the X server from xorg.
Basically all issues are out-of-bounds access or integer underflows in
different request handlers. One CVE is about a leak of uninitialize heap
memory to clients.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2:1.19.2-1+deb9u6.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>For the detailed security status of xorg-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xorg-server">https://security-tracker.debian.org/tracker/xorg-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2359.data"
# $Id: $
