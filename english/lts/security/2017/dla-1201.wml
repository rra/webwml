<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libXcursor, a X cursor management library, is
prone to several heap overflows when parsing malicious files. An
attacker can take advantage of these flaws for arbitrary code execution,
if a user is tricked into processing a specially crafted cursor file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.1.13-1+deb7u2.</p>

<p>We recommend that you upgrade your libxcursor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1201.data"
# $Id: $
