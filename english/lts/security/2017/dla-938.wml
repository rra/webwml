<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Timo Schmid of ERNW GmbH discovered that the Git git-shell, a restricted
login shell for Git-only SSH access, allows a user to run an interactive
pager by causing it to spawn "git upload-pack --help".</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.7.10.4-1+wheezy4.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-938.data"
# $Id: $
