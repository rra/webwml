<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA-993-1 caused regressions for some
applications using Java - including jsvc, LibreOffice and Scilab - due
to the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>. Updated packages are now available to
correct this issue. For reference, the relevant part of the original
advisory text follows.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>

    <p>The Qualys Research Labs discovered that the size of the stack guard
    page is not sufficiently large. The stack-pointer can jump over the
    guard-page and moving from the stack into another memory region
    without accessing the guard-page. In this case no page-fault
    exception is raised and the stack extends into the other memory
    region. An attacker can exploit this flaw for privilege escalation.</p>

    <p>The default stack gap protection is set to 256 pages and can be
    configured via the stack_guard_gap kernel parameter on the kernel
    command line.</p>

    <p>Further details can be found at
    <a href="https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt">https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt</a></p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
3.2.89-2.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.16.43-2+deb8u2.</p>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
4.9.30-2+deb9u2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-993-2.data"
# $Id: $
