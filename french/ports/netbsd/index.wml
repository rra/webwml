#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c" maintainer="Jean-Paul Guillonneau"
"
#############################################################################

<div class="important">
<p><strong>
Cet effort de portage est abandonné depuis longtemps. Il n'a pas été mis à jour
depuis octobre 2002. Les renseignements de cette page sont gardés à titre
historique.
</strong></p>
</div>

<h1>
Debian GNU/NetBSD
</h1>

<p>
Debian GNU/NetBSD (i386) était le portage du système d'exploitation de Debian
sur un noyau et la libc de NetBSD (à ne pas confondre avec les autres portages
BSD sur Debian basés sur la glibc). Au moment de son abandon (aux environs
d’octobre 2002), il était dans sa phase initiale de développement, cependant,
il était installable à partir de rien.
</p>

<p>
Un essai a été aussi fait pour démarrer un portage Debian GNU/NetBSD (alpha)
qui pouvait être exécuté à partir d’un chroot dans un système natif NetBSD
(alpha), mais n’était pas capable d’amorcer tout seul et n’utilisait pas les
bibliothèques natives de NetBSD.
Un <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">
message d’état</a> a été envoyé sur la liste.
</p>

<h2>Nouvelles historiques</h2>

<dl class="gloss">
  <dt class="new">16 octobre 2002 :</dt>
  <dd>
      Des disquettes d’installation expérimentale sont désormais disponibles
      pour installer un système Debian GNU/NetBSD.
  </dd>
  <dt>6 mars 2002 :</dt>
  <dd>
      Matthew a bidouillé <a href="https://packages.debian.org/ifupdown">
      ifupdown</a> pour obtenir un état exploitable.
  </dd>
  <dt>25 février :</dt>
  <dd>
      Matthew a signalé que la prise en charge de shadow et de PAM fonctionne
      désormais sur NetBSD.
      <a href="https://packages.debian.org/fakeroot">fakeroot</a> semble
      fonctionner sur FreeBSD, mais rencontre toujours des problèmes sur NetBSD.
  </dd>
  <dt>7 février 2002 :</dt>
  <dd>
      Nathan vient juste de <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">      publier</a>
      qu’il a obtenu que Debian GNU/FreeBSD amorce en multi-utilisateur. Il
      travaille aussi sur l’installation uniquement de paquets (en utilisant
      un debootstrap bidouillé), ce qui représente une archive considérablement
      plus petite.
  </dd>
  <dt>6 février 2002 :</dt>
  <dd>
      Selon Joel, gcc-2.95.4 a réussi la plus grande partie des tests et est
      empaqueté.
  </dd>
  <dt>6 février 2002 :</dt>
  <dd>X11 fonctionne sur NetBSD ! De nouveau, bravo à Joel Baker.
  </dd>
  <dt>4 janvier 2002 :</dt>
  <dd>Premier stade vers une archive Debian/*BSD : <br />
      <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
      a annoncé</a> une archive <kbd>dupload</kbd>able pour les paquets Debian
      FreeBSD et NetBSD.
  </dd>
  <dt>3 février 2002 :</dt>
  <dd>Debian GNU/NetBSD est désormais
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
      auto-hébergé</a> ! Remarquez qu’il nécessite encore un NetBSD en fonction
      pour l’installation.
  </dd>
  <dt>30 janvier 2002 :</dt>
  <dd>Le portage Debian GNU/*BSD a désormais une page web !</dd>
</dl>

<h2>Pourquoi Debian GNU/NetBSD ?</h2>

<ul>
<li>NetBSD fonctionne sur du matériel non pris en charge par Linux. Porter
Debian vers le noyau NetBSD augmente le nombre de plateformes pouvant
fonctionner sur un système d’exploitation basé sur Debian.</li>

<li>Le projet Debian GNU/Hurd démontre que Debian n’est pas lié à un noyau
spécifique. Cependant, le noyau Hurd est encore plutôt immature — un système
Debian GNU/NetBSD serait utilisable en production.</li>

<li>Les leçons reçues lors du portage de Debian vers NetBSD peuvent être
utilisées pour le portage de Debian vers d’autres noyaux (tels que ceux de <a
href="https://www.freebsd.org/">FreeBSD</a> et <a
href="https://www.openbsd.org/">OpenBSD</a>).</li>

<li>Au contraire de projets tels que
<a href="https://www.finkproject.org/">Fink</a>,
Debian GNU/NetBSD n’a pas pour but de fournir des logiciels supplémentaires ou
un environnement de style Unix vers un système d’exploitation existant (les
arbres de portages *BSD sont déjà complets et ils fournissent incontestablement
un environnement de style Unix). Au lieu de cela, un utilisateur ou un
administrateur, habitué à un système Debian plus traditionnel, se sentira
à l’aise immédiatement avec un système Debian GNU/NetBSD et qualifié dans
un temps relativement court.</li>

<li>Tout le monde n’aime pas l’arbre des portages *BSD ou l’espace utilisateur
des *BSD (il s’agit d’une préférence plutôt qu’une sorte d’appréciation de la
qualité). Des distributions Linux ont été publiées qui fournissent des portages
de style *BSD ou un espace utilisateur de style *BSD pour ceux qui aiment
l’environnement utilisateur, mais qui souhaitent utiliser le noyau Linux.
Debian GNU/NetBSD suit une logique inverse, permettant aux personnes qui
préfèrent l’espace utilisateur GNU ou un système d’empaquetage de style Linux
d’utiliser le noyau NetBSD.</li>

<li>Parce que nous le pouvons.</li>
</ul>

<h2>
Ressources
</h2>

<p>
Il existe une liste de diffusion Debian GNU/*BSD. La plupart des discussions
historiques à propos de ce portage y apparaissent et sont accessibles à partir
des archives web sur <url "https://lists.debian.org/debian-bsd/" />.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
