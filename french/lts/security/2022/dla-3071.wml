#use wml::debian::translation-check translation="e9f0cdab1f1862d5a9c953d4922977c8820e8167" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>libtirpc, une bibliothèque RPC indépendante de la couche de transport, ne
gère pas correctement les connexions TCP suspendues. Un attaquant distant
peut tirer avantage de ce défaut pour provoquer un déni de service.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
1.1.4-0.4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libtirpc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libtirpc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libtirpc">\
https://security-tracker.debian.org/tracker/libtirpc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3071.data"
# $Id: $
