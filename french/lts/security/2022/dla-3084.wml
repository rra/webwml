#use wml::debian::translation-check translation="f4b1d7afd68957a06cdd14cbfcbd530fe5cf0e18" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans ndpi, une bibliothèque
d’inspection en profondeur de paquets.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15472">CVE-2020-15472</a>

<p>Le dissecteur H.323 est vulnérable à une lecture hors limites de tampon
basé sur le tas dans ndpi_search_h323 dans lib/protocols/h323.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15476">CVE-2020-15476</a>

<p>Le dissecteur du protocole Oracle a une lecture hors limites de tampon
basé sur le tas dans ndpi_search_oracle.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 2.6-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ndpi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ndpi, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ndpi">\
https://security-tracker.debian.org/tracker/ndpi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3084.data"
# $Id: $
