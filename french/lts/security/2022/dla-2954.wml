#use wml::debian::translation-check translation="ab372ec580ba390ad42e0d39eb1cda00b009f60b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un problème de divulgation d'informations dans python-treq,
une API et une bibliothèque de haut niveau bibliothèque/API pour effectuer
des requêtes HTTP en utilisant la bibliothèque de programmation réseau
Twisted. Les cookies HTTP n'étaient pas liés à un domaine unique, mais
étaient à la place envoyés à tous les domaines.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23607">CVE-2022-23607</a>

<p>treq est une bibliothèque HTTP inspirée par Requests mais écrite en
se basant sur les Agents de Twisted. Les méthodes de requête de Treq
(« treq.get », « treq.post », etc.) et le constructeur
« treq.client.HTTPClient » acceptent les cookies en tant que dictionnaire.
Ces cookies ne sont pas liés à un domaine unique, et sont donc envoyés à
*tous* les domaines (« supercookies"). Cela peut éventuellement provoquer
la divulgation d'information au moment d'une redirection HTTP à un domaine
différent. Par exemple, « https://example.com » pourrait rediriger vers
« http://cloudstorageprovider.com », ce dernier recevrait le cookie
« session ». Les versions 2021.1.0 et suivantes de Treq lient les cookies
fournis aux méthodes de requête (« treq.request », « treq.get »,
« HTTPClient.request », « HTTPClient.get », etc.) à l'origine du paramètre
*url*. Il est conseillé aux utilisateurs de mettre à niveau leur système
et, pour les utilisateurs qui ne peuvent pas effectuer de mise à niveau, au
lieu de transmettre un dictionnaire comme l'argument de *cookies*, de
transmettre une instance « http.cookiejar.CookieJar » contenant des cookies
avec un domaine et un schéma dont la portée est correctement définie.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 15.1.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-treq.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2954.data"
# $Id: $
