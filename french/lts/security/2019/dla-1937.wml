#use wml::debian::translation-check translation="d12d77f164d6d9656dbb64c796201139283db45f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une ouverture de redirection, permettant à un attaquant d’écrire un fichier
arbitraire en utilisant le nom de fichier fourni et le contenu du
répertoire actuel, en redirigeant une requête provenant de HTTP vers une
URL contrefaite pointant vers un serveur hors de contrôle, a été découverte
et signalée dans <a href="https://security-tracker.debian.org/tracker/CVE-2019-10751">CVE-2019-10751</a>.
Cela a été corrigé en amont et lorsque « --download » sans « --output »
aboutit à une redirection, seule l’URL initiale est désormais prise en
compte, pas la dernière.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 0.8.0-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets httpie.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1937.data"
# $Id: $
