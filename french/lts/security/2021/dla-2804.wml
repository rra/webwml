#use wml::debian::translation-check translation="fa17e417fa131e28052c02c7f8d7765ef5c98f07" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilité ont été corrigé dans libsdl2, la version
ancienne de la bibliothèque Simple DirectMedia Layer qui fournit un accès
de bas niveau à une sortie audio, au clavier, à la souris, à une manette de
jeu et à du matériel graphique</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7572">CVE-2019-7572</a>

<p>Lecture hors limites de tampon dans IMA_ADPCM_nibble dans
audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7573">CVE-2019-7573</a>

<p>Lecture hors limites de tampon basé sur le tas dans InitMS_ADPCM dans
audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7574">CVE-2019-7574</a>

<p>Lecture hors limites de tampon basé sur le tas dans IMA_ADPCM_decode
dans audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7575">CVE-2019-7575</a>

<p>Dépassement de tampon dans MS_ADPCM_decode dans audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7576">CVE-2019-7576</a>

<p>Lecture hors limites de tampon basé sur le tas dans InitMS_ADPCM dans
audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7577">CVE-2019-7577</a>

<p>Lecture hors limites de tampon dans SDL_LoadWAV_RW dans audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7578">CVE-2019-7578</a>

<p>Lecture hors limites de tampon basé sur le tas dans InitIMA_ADPCM dans
audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>

<p>Lecture hors limites de tampon basé sur le tas dans Blit1to4 dans
video/SDL_blit_1.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7636">CVE-2019-7636</a>

<p>Lecture hors limites de tampon basé sur le tas dans SDL_GetRGB dans
video/SDL_pixels.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>

<p>Dépassement de tampon basé sur le tas dans SDL_FillRect dans
video/SDL_surface.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7638">CVE-2019-7638</a>

<p>Lecture hors limites de tampon basé sur le tas dans Map1toN dans
video/SDL_pixels.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13616">CVE-2019-13616</a>

<p>Lecture hors limites de tampon basé sur le tas dans BlitNtoN dans
video/SDL_blit_N.c</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.2.15+dfsg1-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl1.2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libsdl1.2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libsdl1.2">\
https://security-tracker.debian.org/tracker/libsdl1.2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2804.data"
# $Id: $
