# translation of ports.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml ports\n"
"PO-Revision-Date: 2013-02-26 23:09+0100\n"
"Last-Translator: Lev Lamberov\n"
"Language-Team: Russian <>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian для Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian для PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Компакт-диски Hurd"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian для IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Как связаться"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "Процессоры"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Благодарности"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Разработка"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Документация"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Установка"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Настройка"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Ссылки"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Новости"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Перенос"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Переносы"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Проблемы"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Карта программного обеспечения"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Состояние"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Источник питания"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Системы"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD для i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD для Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Зачем"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Люди"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian для PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian для Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian для переносных компьютеров"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian для Sparc64"

#~ msgid "Debian for S/390"
#~ msgstr "Debian для S/390"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian для MIPS"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian для Motorola 680x0"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Main"
#~ msgstr "Основная"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian для Beowulf"

#~ msgid "Debian for ARM"
#~ msgstr "Debian для ARM"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian для 64-разрядной архитектуры AMD"
